Pod::Spec.new do |s|
  s.name     = 'capsoul'
  s.version  = '1.0.0'
  s.summary  = 'Collection of handy snippets.'
  s.homepage = 'https://www.yy.com'
  s.authors  = { 'Charles Lee' => 'liyuhua@yy.com' }
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.source   = { :git => "https://gitlab.com/liyuhua/capsoul.git", :tag => s.version }

  s.ios.deployment_target = '8.0'

  s.source_files = 'snippets/**/*'
  s.header_mappings_dir = '.'
end
