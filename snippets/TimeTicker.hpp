/*
MIT License

Copyright (c) 2016 charles.liyh@google.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <thread>
#include <chrono>
#include <functional>
#include <cassert>
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable

namespace capsoul {

    class TimerTicker {
    public:
        TimerTicker(int perMillis) {
            this->interval = perMillis;
        }

        ~TimerTicker() {
            if (running) {
                // 编程习惯不好的程序员经常会不对称地使用 start/stop 方法，这种行为经常会带来不可预期的程序行为。但是溺爱
                // 用户，帮他们在这里调用stop不是什么优秀的解决方案，反而会导致他们意识不到自己的责任所在。从而写出垃圾代码。
                printf("WARNING! TIMERTICKER SHOULD BE STOPPED BEFORE DESTROYED, OTHERWISE IT WOULD CRASH!\n");
            }

            // 即使这里不抛异常，线程处理中也会因为对象已被释放而导致野指针访问，进而触发异常。反而，主动assert有利于问题发生点的定位与拍错
            assert(!running);
        }

        void start(std::function<void ()> action) {
            assert(!running);

            this->action = action;
            running = true;

            loopThread = std::thread([this] { run(); });
        }

        void stop() {
            assert(running);
            // 将running设置为false很可能不会立即终止线程，而是等待ticker线程在完成一轮sleep之后才得以正常终止
            // 这意味着，interval越大，stop的同步等待时间越长，反之越短。但是interval过短，会导致系统资源消耗过多。
            // 因此，应根据实际需要，找到一个合适的interval来平衡这两个因素。
            {
                std::unique_lock<mutex> lck(mtx);
                running = false;
                cv.notify_all();
            } // 不要轻易移除这个花括号，它决定了lck的生命周期和作用范围，避免在lock的时候执行join操作，从而导致死锁

            loopThread.join(); // wait until it stops safely
        }

    private:

        void run() {
            // 为了避免误差累积，这里没有简单地使用sleep_for方式进行时间控制，而是使用了condition_variable::wait_until
            std::chrono::high_resolution_clock::time_point last = std::chrono::high_resolution_clock::now();
            while(running) {
                std::chrono::high_resolution_clock::time_point next = last + std::chrono::milliseconds(interval);

                // 如果使用sleep_until会导致interval较长（例如3s）的ticker，在stop时会有很长的等待期。例如CatcherTests中，
                // 每个验收用例都可能需要等待3s才能结束。所以，这里使用了condition_variable::wait_until同时支持信号量式的通知
                // 处理，也支持超时处理这种特性，保证了既可以用较高精度进行时间延时控制，也可以保证在任意时间终止线程
                std::unique_lock<mutex> lck(mtx);
                cv.wait_until(lck, next);

                if (!running) {
                    // 有可能在sleep的过程中调用stop操作，直接break可以尽早结束线程，同时避免无意义(stop之后)的action操作
                    break;
                }

                action();
                last = next;
            }
        }

        std::condition_variable cv;
        std::mutex              mtx;

        int  interval;
        bool running = false;
        std::function<void ()> action;
        std::thread            loopThread;
    };

}